module QuotesHelper
  def quote_params
    params.require(:quote).permit(:citation)
  end
end
