class QuotesController < ApplicationController
  include QuotesHelper
  def index
    @quotes = Quote.all
  end
  def new

  end
  def create
    @quote = Quote.new(quote_params)
    @quote.save
    redirect_to quotes_path
  end
end
